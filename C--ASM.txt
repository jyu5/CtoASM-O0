sum2:
        sub     sp, sp, #8		//8 bytes are reserved on the stack pointer so it can perform the sum
        str     r0, [sp, #4]		//stores r0 at adress (sp + 4) so it can be loaded if needed
        str     r1, [sp]		//stores r1 at address (sp) so it can be loaded if needed
        ldr     r0, [sp, #4]		//loads the value pointed to by (sp + 4) and places it in r0 as a paramemter to perform sum
        ldr     r1, [sp]		//loads the value pointed to by (sp) and places it in r1 as a paramemter to perform sum
        add     r0, r0, r1		//r0 = r0 + r1
        add     sp, sp, #8		//8 bytes are increased on the stack pointer to clear the space used.
        bx      lr			//return function
print_the_value:
        push    {r11, lr}		//saving frame pointer and LR onto the stack so it can be restored later 
        mov     r11, sp			//move stack pointer to frame pointer to set the region of memory used for this function.
        sub     sp, sp, #8		//8 bytes are reserved on the stack pointer to perform this function
        str     r0, [sp, #4]		//stores r0 at address (sp + 4) so it can be loaded if needed
        ldr     r1, [sp, #4]		//loads the value pointed to by (sp + 4) and places it in r1 as a parameter to print
        ldr     r0, .LCPI1_0		//loads the value pointed to by .LCPI1_0 and places it in r0 as a parameter to print
        bl      printf                  //call printf
        mov     sp, r11			//move frame pointer to stack pointer to exit the function
        pop     {r11, lr}		//restoring frame pointer from the stack
        bx      lr			//return function
.LCPI1_0:
        .long   .L.str
entry_point:
        push    {r11, lr}		//saving frame pointer and LR onto the stack so it can be restored later.
        mov     r11, sp			//move stack pointer to frame pointer to set the region of memory used for this function.
        sub     sp, sp, #16		//16 bytes are reserved on the stack pointer to perform this function
        bl      rand			//(int a) r0 = rand; call rand on int a
        str     r0, [sp, #8]		//stores (int a) r0 at address (sp + 8) so it can be loaded if needed
        bl      rand			//(int b) r0 = rand; call rand on int b
        str     r0, [sp, #4]		//store (int b) r0 at address (sp + 4) so it can be loaded if needed
        ldr     r0, [sp, #4]		//reloads the value pointed to by (sp + 4) (address of int b) and places it in r0 
                                           to pass as the first parameter of sum2.
        ldr     r1, [sp, #8]		//reloads the value pointed to by (sp + 8) (address of int a) and places it in r1 
                                           to pass as the second parameter of sum2.
        bl      sum2                    //call sum2 on the parameters that was just passed
        str     r0, [sp]		//store r0 (int result) at address (sp)
        ldr     r0, [sp]		//reloads the value pointed to by (sp) (address of result) and places it in r0 to pass as a paramemter to
                                          print
        bl      print_the_value         //call print_the_value 
        ldr     r0, [r11, #-4]		//loads the value pointed to by (r11 + 4) and places it to r0, the value at (r11 + 4) is some random
                                          data because the function needs to return something
        mov     sp, r11			//move frame pointer to stack pointer to exit the function
        pop     {r11, lr}		//restoring frame pointer from the stack
        bx      lr			//return function
.L.str:
        .asciz  "%d"			//NUL character with byte value 0
